<?php

namespace Drupal\skype_multicall\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Class SkypeMulticallFormBlock.
 *
 * @Block(
 *   id = "skype_multicall_skypemulticallform",
 *   admin_label = @Translation("Skype multicall Form")
 * )
 */
class SkypeMulticallFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Drupal\Core\Form\FormBuilderInterface definition.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;


  /**
   * SkypeMulticallForm constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $currentRouteMatch
   *   Route Match interface.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   Form Builder Interface.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              RouteMatchInterface $currentRouteMatch,
                              FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $currentRouteMatch;
    $this->formBuilder = $formBuilder;
  }


  /**
   * Create Function.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   ContainerInterface object.
   * @param array $configuration
   *   Configuration array.
   * @param $plugin_id
   *   Id of Plugin.
   * @param $plugin_definition
   *   Definition of plugin.
   *
   * @return \Drupal\skype_multicall\Plugin\Block\SkypeMulticallForm
   *
   *   {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('form_builder')
    );
  }

  /**
   * Build html Block.
   *
   * @return array
   *
   *   {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm('Drupal\skype_multicall\Form\SkypeMulticallForm');
  }

}
