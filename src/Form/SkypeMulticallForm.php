<?php

namespace Drupal\skype_multicall\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Implements the SkypeMulticallForm form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SkypeMulticallForm extends FormBase {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Request object.
   *
   * @var null|\Symfony\Component\HttpFoundation\Request
   *  Http Request.
   */
  protected $request;

  /**
   * The entity type manager service.
   *
   * We need this for the submit handler.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * SkypeMulticallForm constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(AccountInterface $current_user,
                               Connection $database,
                               EntityTypeManagerInterface $entity_type_manager) {
    $this->currentUser = $current_user;
    $this->database = $database;
    /** @var \Symfony\Component\HttpFoundation\RequestStack $request_stack */
    $request_stack = \Drupal::requestStack();
    $this->request = $request_stack->getCurrentRequest();
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Create function.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\Core\Form\FormBase|\Drupal\skype_multicall\Form\SkypeMulticallForm
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Build the form SkypeMulticallForm.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('skype_multicall.settings');
    $field_skype_id = $config->get('field_skype_id');


    $form['#attached']['library'][] = ($this->request->isSecure()) ? 'skype/skype.library.secure' : "skype/skype.library";
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'skype_multicall/skype_multicall.form_styles';

    $form['#prefix'] = '<div id="form-multicall-container">';
    $form['#suffix'] = '</div>';

    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['field_topic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Topic'),
      '#required' => TRUE,
      '#rows' => 5,
    ];
    $form['users'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#tags' => TRUE,
      '#title' => $this->t('Choose users to call (Separate with commas)'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $disabled_button = FALSE;
    if (empty($field_skype_id)) {
      $disabled_button = TRUE;
      $this->messenger()->addMessage($this->t("Information about Skype is missing. Contact the administrator."), "warning");
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#disabled' => $disabled_button,
      '#ajax' => [
        'callback' => '::skypebuttonCallback',
        'event' => 'click',
      ],
      '#value' => $this->t('Create meeting'),
    ];

    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * @return string
   */
  public function getFormId() {
    return 'skype_multi_call_form';
  }

  /**
   * Implements form validation.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $state_users = $form_state->getValue('users');
    if (empty($state_users)) {
      $form_state->setErrorByName('users', 'Please select at least one user.');
    }
  }

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Callback button skype.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Return command AjaxCallback modal
   */
  public function skypebuttonCallback(array &$form, FormStateInterface $form_state) {


    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#form-multicall-container', $form));
    }
    else {

      $field_topic = $form_state->getValue('field_topic');
      $users_selected = $form_state->getValue('users');
      $users_name = $participants = [];

      try {
        $config = $this->config('skype_multicall.settings');
        $field_skype_id = $config->get('field_skype_id');
        $types_field_list = user_type_field_skype();
        if (isset($types_field_list[$field_skype_id])) {
          foreach ($users_selected as $user) {
            $uid = $user['target_id'];
            $user_object = $this->entityTypeManager->getStorage('user')->load($uid);
            if ($user_object) {
              $users_name[] = $user_object->getDisplayName();
              $participants[] = $user_object->get($field_skype_id)->value;
            }
          }
        }

      }
      catch (\Exception $e) {
        $this->messenger()->addMessage($this->t('An unexpected error occurred. Please contact the administrator.'), 'error');
        $error = $this->t("An unexpected error occurred while getting users : ") . $e->getMessage();
        \Drupal::logger('skype_multicall')->error($error);
      }


      if (empty($participants)) {
        $this->messenger()->addMessage($this->t('An unexpected error occurred while getting data from Skype users. Please contact the administrator.'), 'error');
      }
      else {
        $this->messenger()->addMessage($this->t('Skype call for users: %users', ['%users' => implode(', ', $users_name)]));

        $theme_table = [
          '#theme' => 'skype_button_multicall',
          '#participants' => $participants,
          '#topic' => str_replace(' ', '%20', $field_topic),
        ];
        $html_button = \Drupal::service('renderer')->render($theme_table);
        $response->addCommand(new OpenModalDialogCommand("Buttons Skype", $html_button, ['width' => 400]));
      }

      $response->addCommand(new ReplaceCommand('#form-multicall-container', $form));
    }

    return $response;

  }

}

