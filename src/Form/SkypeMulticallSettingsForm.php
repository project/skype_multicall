<?php

namespace Drupal\skype_multicall\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SkypeMulticallSettingsForm
 *
 * @package Drupal\skype_multicall\Form
 */
class SkypeMulticallSettingsForm extends ConfigFormBase {

  /**
   * @return string
   */
  public function getFormId() {
    return 'skype_multicall_admin_settings';
  }

  /**
   * Function getEditableConfigNames().
   * @return array
   */
  protected function getEditableConfigNames() {
    return ['skype_multicall.settings'];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('skype_multicall.settings');
    $default_value = $config->get('field_skype_id');
    $types_list = user_type_field_skype();
    $options_field_skype = [];
    if (!empty($types_list)) {
      foreach ($types_list as $key => $value) {
        $options_field_skype[$key] = $key;
      }
    }

    if (empty($options_field_skype)) {
      $this->messenger()->addMessage($this->t("You must create at least one skype type field in the user account profile to be able to select an option."), "warning");
    }

    $form['field_skype_id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Field types allowed'),
      '#options' => $options_field_skype,
      '#description' => $this->t('Select a Skype field type for the user entity.'),
      '#required' => TRUE,
      '#default_value' => (empty($default_value)) ? [] : $default_value,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field_skype_id = $form_state->getValue('field_skype_id');
    $this->config('skype_multicall.settings')
      ->set('field_skype_id', $field_skype_id)
      ->save();
    parent::submitForm($form, $form_state);
  }
}