Description
-----------
This is simple module that allows to create multi calls to different Skype users, extending the functionality of the Skype module.
It provides a page and a block, which it allows you to create a multi-group Skype call from the Drupal user selection and the subject of the call.

Installation/configuration
------------
To install/configure this module, do the following:

1. Extract the compress file that you downloaded from Drupal.org.
2. Install as you would normally install a contributed Drupal module. See: https://drupal.org/documentation/install/modules-themes/modules-8 for further information.
3. Add a field of type "Skype" in the field management of the user account, in admin/config/people/accounts/fields.
4. Go to /admin/config/user-interface/skype-multicall  and select the field created above.
5. Create multiple users by setting the value of the skype id in the created field.
6. Navigate to /skype-multicall and create the call.(write topic and users). Enjoy!!